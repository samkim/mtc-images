FROM node:10-slim

# TODO: Preload and build dependencies
RUN apt-get update -qq && apt-get install -y --no-install-recommends \
	git && \
	apt-get clean && \
	rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

CMD [ "node" ]
